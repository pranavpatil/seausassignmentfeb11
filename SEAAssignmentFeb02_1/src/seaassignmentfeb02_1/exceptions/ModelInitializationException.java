package seaassignmentfeb02_1.exceptions;

public class ModelInitializationException extends Exception{

	private String specificMessage;
	public ModelInitializationException(String specificMsg)
	{
		specificMessage = specificMsg;
	}
	
	public String getSpecificMessage()
	{
		return specificMessage;
	}
}
