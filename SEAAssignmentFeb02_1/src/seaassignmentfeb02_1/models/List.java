
package seaassignmentfeb02_1.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class List {

    @SerializedName("msgTypeText")
    @Expose
    private String msgTypeText;
    @SerializedName("msgType")
    @Expose
    private String msgType;
    @SerializedName("timeStamp")
    @Expose
    private String timeStamp;
    @SerializedName("msgId")
    @Expose
    private String msgId;
    @SerializedName("msgSystem")
    @Expose
    private String msgSystem;
    @SerializedName("severity")
    @Expose
    private String severity;
    @SerializedName("msgText")
    @Expose
    private String msgText;
    @SerializedName("msgRef")
    @Expose
    private String msgRef;
    @SerializedName("user")
    @Expose
    private String user;
    @SerializedName("job")
    @Expose
    private String job;
    @SerializedName("jobNbr")
    @Expose
    private String jobNbr;
    @SerializedName("replyStatus")
    @Expose
    private String replyStatus;
    @SerializedName("replyText")
    @Expose
    private String replyText;
    @SerializedName("msgLvl02Text")
    @Expose
    private String msgLvl02Text;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("entryInquiry")
    @Expose
    private Boolean entryInquiry;

    public String getMsgTypeText() {
        return msgTypeText;
    }

    public void setMsgTypeText(String msgTypeText) {
        this.msgTypeText = msgTypeText;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public String getMsgSystem() {
        return msgSystem;
    }

    public void setMsgSystem(String msgSystem) {
        this.msgSystem = msgSystem;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public String getMsgText() {
        return msgText;
    }

    public void setMsgText(String msgText) {
        this.msgText = msgText;
    }

    public String getMsgRef() {
        return msgRef;
    }

    public void setMsgRef(String msgRef) {
        this.msgRef = msgRef;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getJobNbr() {
        return jobNbr;
    }

    public void setJobNbr(String jobNbr) {
        this.jobNbr = jobNbr;
    }

    public String getReplyStatus() {
        return replyStatus;
    }

    public void setReplyStatus(String replyStatus) {
        this.replyStatus = replyStatus;
    }

    public String getReplyText() {
        return replyText;
    }

    public void setReplyText(String replyText) {
        this.replyText = replyText;
    }

    public String getMsgLvl02Text() {
        return msgLvl02Text;
    }

    public void setMsgLvl02Text(String msgLvl02Text) {
        this.msgLvl02Text = msgLvl02Text;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getEntryInquiry() {
        return entryInquiry;
    }

    public void setEntryInquiry(Boolean entryInquiry) {
        this.entryInquiry = entryInquiry;
    }
    
    @Override
    public boolean equals(Object obj)
    {
    	if(obj ==null)
    	{
    		return false;
    	}
    	if(obj instanceof List)
    	{
    		List list = (List) obj;
    		return jobNbr.equals(list.getJobNbr());
    	}
    	else
    	{
    		return false;
    	}

    	
    }

}
