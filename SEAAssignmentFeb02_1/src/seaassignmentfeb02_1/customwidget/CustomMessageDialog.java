package seaassignmentfeb02_1.customwidget;

import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class CustomMessageDialog extends TitleAreaDialog {

    private Text txtMessageWidget;
    private String messageText;

    public CustomMessageDialog(Shell parentShell) {
        super(parentShell);
    }
    
    public void setTitleAndMessage(String title, String message)
    {
        setTitle(title);
        setMessage(message, IMessageProvider.INFORMATION);   	
    }

    @Override
    public void create() 
    {
        super.create();
    }

    @Override
    protected Control createDialogArea(Composite parent) {
        Composite dialogArea = (Composite) super.createDialogArea(parent);
        Composite container = new Composite(dialogArea, SWT.NONE);
        container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        GridLayout layout = new GridLayout(1, true);
        container.setLayout(layout);
        
        GridData dataReplyContent = new GridData();
        dataReplyContent.grabExcessHorizontalSpace = true;
        dataReplyContent.grabExcessVerticalSpace = true;
        dataReplyContent.horizontalAlignment = GridData.FILL;
        dataReplyContent.verticalAlignment = GridData.FILL;

        txtMessageWidget = new Text(container, SWT.BORDER);
        txtMessageWidget.setLayoutData(dataReplyContent);
        return dialogArea;
    }


    @Override
    protected boolean isResizable() {
        return true;
    }

    // save content of the Text fields because they get disposed
    // as soon as the Dialog closes
    private void saveInput() {
        messageText = txtMessageWidget.getText();
    }

    @Override
    protected void okPressed() {
        saveInput();
        super.okPressed();
    }

    public String getMessage() {
        return messageText;
    }

 
}