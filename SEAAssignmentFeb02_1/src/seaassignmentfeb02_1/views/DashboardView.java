package seaassignmentfeb02_1.views;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.*;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.experimental.chart.swt.ChartComposite;
import org.jfree.util.Rotation;

import seaassignmentfeb02_1.exceptions.ModelInitializationException;
import seaassignmentfeb02_1.listeners.PropertyChangeListener;
import seaassignmentfeb02_1.models.ChartModel;
import seaassignmentfeb02_1.models.InMemoryModelForViews;
import seaassignmentfeb02_1.utils.ChartModelAdaptor;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import javax.inject.Inject;

public class DashboardView extends ViewPart {

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "seaassignmentfeb02_1.views.DashboardView";

	@Inject
	IWorkbench workbench;

	private JFreeChart messageTypeDistributionChart, replyDistributionChart;
	private DefaultCategoryDataset messageTypeDistributionDataSet = new DefaultCategoryDataset();
	private DefaultPieDataset pieDataSet = new DefaultPieDataset();

	@Override
	public void createPartControl(Composite parent) {

		try {
			InMemoryModelForViews.getInstance().initialize();

			messageTypeDistributionChart = createMessageTypeDistributionChart();
			new ChartComposite(parent, SWT.NONE, messageTypeDistributionChart, true);

			replyDistributionChart = createReplyDistributionChart();
			new ChartComposite(parent, SWT.NONE, replyDistributionChart, true);

			/**
			 * This uses an observer pattern where this listnener is registered with the
			 * model and when the model is changed a valuechanged method is triggered. For
			 * now I am not passing any value - it is just to trigger a rerender of the
			 * entire chart.
			 */
			InMemoryModelForViews.getInstance().addPropertyChangeListener(new PropertyChangeListener() {
				@Override
				public void valueChanged(Object value) {

					ChartModel messageTypeDistributionChartModel = ChartModelAdaptor
							.getMessageTypeDistributionChartModel();
					populateDatasetForMessageTypeDistributionChart(messageTypeDistributionChartModel);
					ChartModel replyDistributionChartModel = ChartModelAdaptor.getReplyDistributionChartModel();
					populateDataSetForReplyDistributionChart(replyDistributionChartModel);

				}
			});

		} catch (ModelInitializationException exp) {
			Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
			MessageDialog errorMessage = new MessageDialog(shell, "Error", null, exp.getSpecificMessage(),
					MessageDialog.ERROR, new String[] { "OK" }, 0);

			errorMessage.open();
		}

	}

	@Override
	public void setFocus() {

	}

	private void populateDatasetForMessageTypeDistributionChart(ChartModel messageTypeDistributionChartModel) {

		String series[] = messageTypeDistributionChartModel.getCategorySeries();
		double[] values = messageTypeDistributionChartModel.getyValues();

		messageTypeDistributionDataSet.clear();

		for (int count = 0; count < series.length; count++) {
			messageTypeDistributionDataSet.addValue(values[count], messageTypeDistributionChartModel.getxAxisText(),
					series[count]);
		}

	}

	private org.jfree.chart.JFreeChart createMessageTypeDistributionChart() {

		ChartModel messageTypeDistributionChartModel = ChartModelAdaptor.getMessageTypeDistributionChartModel();
		populateDatasetForMessageTypeDistributionChart(messageTypeDistributionChartModel);

		final JFreeChart chart = ChartFactory.createBarChart3D(messageTypeDistributionChartModel.getTitle(),
				messageTypeDistributionChartModel.getxAxisText(), messageTypeDistributionChartModel.getyAxisText(),
				messageTypeDistributionDataSet);
		return chart;
	}

	private void populateDataSetForReplyDistributionChart(ChartModel replyDistributionChartModel) {
		String series[] = replyDistributionChartModel.getCategorySeries();
		double[] values = replyDistributionChartModel.getyValues();
		pieDataSet.setValue(series[0], values[0]);
		pieDataSet.setValue(series[1], values[1]);

	}

	private org.jfree.chart.JFreeChart createReplyDistributionChart() {

		ChartModel replyDistributionChartModel = ChartModelAdaptor.getReplyDistributionChartModel();
		populateDataSetForReplyDistributionChart(replyDistributionChartModel);

		final JFreeChart chart = ChartFactory.createPieChart3D(replyDistributionChartModel.getTitle(), pieDataSet, true,
				true, false);
		final PiePlot3D plot = (PiePlot3D) chart.getPlot();
		plot.setStartAngle(290);
		plot.setDirection(Rotation.CLOCKWISE);
		plot.setForegroundAlpha(0.5f);
		return chart;
	}

}
