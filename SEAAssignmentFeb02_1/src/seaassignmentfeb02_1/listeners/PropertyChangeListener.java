package seaassignmentfeb02_1.listeners;

/**
 * 
 * @author pranav
 * Part of the observer pattern where the listeners are registered with the model 
 * and get triggered when a value changes
 *
 */
public abstract class PropertyChangeListener 
{
	public abstract void valueChanged(Object value);
}
